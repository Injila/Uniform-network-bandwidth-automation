#ifndef INTERFACE_H // include guard
#define INTERFACE_H
#include <string>
namespace UNBA {
    class Interface
    {
        public:
            std::string getInterface();
        };
    } // namespace UNBA

#endif