#ifndef SUBNET_MASK_H // include guard
#define SUBNET_MASK_H
#include <string>
namespace UNBA {
  class SubnetMask {
  public:
    std::string netmask();
    std::string countBits(std::string mask);
  };
}

#endif
