#ifndef IP_ADDRESS_H // include guard
#define IP_ADDRESS_H
#include <string>
namespace UNBA {
  class IPAddress {
  public:
    std::string getIPAddress();
  };
}

#endif
