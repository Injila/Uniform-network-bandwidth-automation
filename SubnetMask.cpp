#include "SubnetMask.h"
#include<iostream>
#include <string.h>
#include <arpa/inet.h>
#include <sys/socket.h>
#include <ifaddrs.h>
#include <stdio.h>
#include "Interface.h"

using namespace UNBA;
using namespace std;

string SubnetMask::netmask() {
  Interface interface;
  string interfaceName = interface.getInterface();
  //We'll need a char * for string functions. Allocating this way as c_str() returns a const char *.
  char * cInterfaceName = new char[interfaceName.length()+1];
  strcpy(cInterfaceName, interfaceName.c_str());
  static int counter = 0;
  struct ifaddrs *ifap, *ifa;
  struct sockaddr_in *sa;
  string addr;

  getifaddrs (&ifap);
  for (ifa = ifap; ifa; ifa = ifa->ifa_next) {
      if (ifa->ifa_addr->sa_family==AF_INET) {
          sa = (struct sockaddr_in *) ifa->ifa_netmask;
          if(strcmp(cInterfaceName, ifa->ifa_name)==0) {
            addr = inet_ntoa(sa->sin_addr);
            return addr;
          }
      }
  }

  freeifaddrs(ifap);
  return addr;
}

string SubnetMask::countBits(string mask) {
  string maskBits;
  int count = 32;
  int n = mask.length();
  for(int i=0; i<n; i++) {
    if(mask[i] == '0') {
      count = count - 8;
    }
  }
  maskBits = to_string(count);
  return maskBits;
}
