/*
    This file gets the current interface
 */

#include "Interface.h"
#include <iostream>     ///< cout
#include <cstring>      ///< memset
#include <errno.h>      ///< errno
#include <sys/socket.h> ///< socket
#include <netinet/in.h> ///< sockaddr_in
#include <ifaddrs.h>    ///< getifaddrs
#include <arpa/inet.h>  ///< getsockname
#include <unistd.h>     ///< close
#include <string>

using namespace UNBA;
using namespace std;

string Interface::getInterface()
{
    string result;
    const char *google_dns_server = "8.8.8.8";
    struct sockaddr_in serv;
    int dns_port = 53;

    int sock = socket(AF_INET, SOCK_DGRAM, 0);

    if (sock < 0)
    {
        std::cout << "Socket error" << std::endl;
    }

    memset(&serv, 0, sizeof(serv));
    serv.sin_family = AF_INET;
    serv.sin_addr.s_addr = inet_addr(google_dns_server);
    serv.sin_port = htons(dns_port);

    int err = connect(sock, (const struct sockaddr *)&serv, sizeof(serv));
    if (err < 0)
    {
        std::cout << "Error number: " << errno
                  << ". Error message: " << strerror(errno) << std::endl;
    }

    struct sockaddr_in addr;
    struct ifaddrs *ifaddr;
    struct ifaddrs *ifa;
    socklen_t addr_len;

    addr_len = sizeof(addr);
    int x = getsockname(sock, (struct sockaddr *)&addr, &addr_len);
    int y = getifaddrs(&ifaddr);

    char addr_1[16];
    inet_ntop(AF_INET, &addr.sin_addr, addr_1, 16);

    // look which interface contains the wanted IP.
    // When found, ifa->ifa_name contains the name of the interface (eth0, eth1, ppp0...)

    for (ifa = ifaddr; ifa != NULL; ifa = ifa->ifa_next)
    {
        // std::cout << ifa->ifa_name << std::endl;
        if (ifa->ifa_addr && ifa->ifa_addr->sa_family == AF_INET)
        {
            // std::cout << ifa->ifa_name << std::endl;
            struct sockaddr_in *inaddr = (struct sockaddr_in *)ifa->ifa_addr;
            if (inaddr->sin_addr.s_addr == addr.sin_addr.s_addr && ifa->ifa_name)
            {
                result = ifa->ifa_name;
            }
        }
    }
    freeifaddrs(ifaddr);

    return result;
}
