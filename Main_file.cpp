#define STD_INPUT  0    /* file descriptor for standard input  */
#define STD_OUTPUT 1    /* file descriptor for standard output */
#define READ  0         /* read file descriptor from pipe  */
#define WRITE 1

#include "IPAddress.h"
#include "SubnetMask.h"
#include "Interface.h"
#include <iostream>
#include <sstream>
#include <string>
#include <cstring>
#include <unistd.h>
#include <wait.h>
#include <fcntl.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fstream>
#include <dirent.h>


using namespace UNBA;

//Checks if a process if running.
bool isRunning(const char* name)
 {
   char command[32];
   sprintf(command, "pgrep %s > /dev/null", name);
   return 0 == system(command);
 }

//Returns the pid of the process by giving nameof the process.
 int getProcIdByName(std::string procName)
 {
     int pid = -1;

     // Open the /proc directory
     DIR *dp = opendir("/proc");
     if (dp != NULL)
     {
         // Enumerate all entries in directory until process found
         struct dirent *dirp;
         while (pid < 0 && (dirp = readdir(dp)))
         {
             // Skip non-numeric entries
             int id = atoi(dirp->d_name);
             if (id > 0)
             {
                 // Read contents of virtual /proc/{pid}/cmdline file
                 std::string cmdPath = std::string("/proc/") + dirp->d_name + "/cmdline";
                 std::ifstream cmdFile(cmdPath.c_str());
                 std::string cmdLine;
                 getline(cmdFile, cmdLine);
                 if (!cmdLine.empty())
                 {
                     // Keep first cmdline item which contains the program path
                     size_t pos = cmdLine.find('\0');
                     if (pos != std::string::npos)
                         cmdLine = cmdLine.substr(0, pos);
                     // Keep program name only, removing the path
                     pos = cmdLine.rfind('/');
                     if (pos != std::string::npos)
                         cmdLine = cmdLine.substr(pos + 1);
                     // Compare against requested process name
                     if (procName == cmdLine)
                         pid = id;
                 }
             }
         }
     }

     closedir(dp);

     return pid;
 }

//Converts the host number string to int
int captureNumberValue(std::string host_str) {
  int result;
  std::string temp="";
  for(int i=0; i<host_str.size(); i++) {
    if(host_str[i] >= '0' && host_str[i] <= '9') {
      temp += host_str[i];
    }
  }
  result = stoi(temp);
  return result;
}

/* join two commands by pipe */
int join( char *com1[], char *com2[] ) {
   int pipefd[2];
   int pid;

   // make a pipe (fds go in pipefd[0] and pipefd[1])
    pipe(pipefd);
    int status;     /* status */

    /* create child to run commands */
    switch( fork() ) {
        case -1:
            /* fork was unsuccessful */
            std::cout<<"unable to fork!"<<std::endl;
            return( -1 );

        case 0:
            /* child process */
            break;

        default:
            /* parent process */
            wait( &status );
            return( status );
    }

    /* remainder of routine executed by child */

    /* create pipe */
    if( pipe( pipefd ) < 0 ) /* can use 'p' since address */ {
        std::cout<<"unable to pipe!"<<std::endl;
        return( -1 );
    }

    /* create another process */
    pid = fork();

    if (pid == 0)
      {
        dup2(pipefd[0], 0);
        int file_out = open("out.txt", O_WRONLY | O_TRUNC | O_CREAT, S_IRUSR | S_IRGRP | S_IWGRP | S_IWUSR);

        dup2(file_out,1);
        // close unused hald of pipe
        close(pipefd[1]);

        // execute grep
        execvp(com2[0], com2);
        close(file_out);
      }
    else
      {
        // parent process
        dup2(pipefd[1], 1);

        // close unused unput half of pipe
        close(pipefd[0]);

        // execute nmap
        execvp(com1[0], com1);
      }
}


int main() {
  std::cout<<"Running this script would cause some downloading applications to die..."<<std::endl;
  std::cout<<"Checking the compatibility with your system."<<std::endl;
  if (system(NULL)) {
       std::cout << "Command processor exists. Continuing..."<<std::endl;
       std::string IPAddr;
       std::string mask;
       std::string maskBits;
       std::string networkAddress;
       std::string activeInterface;
       bool processKilled=false;
       int killedProcessID;
       bool firstKill=false;
       unsigned long microseconds=500000000;

       try {
         IPAddress localIP;
         SubnetMask networkMask;
         Interface interface;
         IPAddr = localIP.getIPAddress();
         mask = networkMask.netmask();
         maskBits = networkMask.countBits(mask);
         activeInterface = interface.getInterface();
         networkAddress = IPAddr+"/"+maskBits;
         }
         catch(std::bad_alloc & ba) {
            std::cout<<"Program cannot allocate memory. Try again after closing some programs or run in a priviledged mode."<<std::endl;
            return 1;
          }
         std::cout<<"Captured address "<<networkAddress<<std::endl;
         std::cout << "Captured interface " << activeInterface << std::endl;
         /* Using general nmap host discovery without any parameter, this will send -PE(ICMP echo) -PS443 (TCP SYN)
         -PA80 (TCP ACK) -PP (ICMP timestamp) for a priviledged user and only -PS80, 443 (TCP SYN) for
         unpriviledged users */
         int n = networkAddress.length();
         char networkAddressChar[n+1];
         std::strcpy(networkAddressChar, networkAddress.c_str());
         char *args[] = {"nmap", networkAddressChar, "-oG", "-", (char *) NULL};
         char *args1[] ={"grep", "-oh", "(.*hosts", (char *)NULL};

         while(true) {
           if(firstKill) {
             std::cout<<"(Yawn) Script is up again to have a look at your system."<<std::endl;
           }
         int pid = join(args, args1);
         std::ifstream inFile;
         inFile.open("out.txt"); //open the input file

         std::stringstream strStream;
         strStream << inFile.rdbuf(); //read the file

         std::string str_host = strStream.str(); //str holds the content of the file
         inFile.close();
         int num = captureNumberValue(str_host);
         std::cout<<"Printing the number of hosts up: "<<num<<std::endl;
         if(isRunning("transmission-gt")) {
           std::cout<<"A downloading process is running"<<std::endl;
           std::cout<<"Fetching its pid...";
           int pid_process = getProcIdByName("transmission-gtk");
           std::cout<<pid_process<<std::endl;

             if(num > 1) {
               std::cout<<"As there are multiple hosts up, killing the downloading process"<<std::endl;
               if(kill(pid_process, SIGSTOP)==0) {
                 std::cout<<"Successfully paused the downloading process."<<std::endl;
                 killedProcessID=pid_process;
                 processKilled=true;
                 firstKill=true;
               }
               else {
                 std::cout<<"Cannot pause the process."<<std::endl;
               }
               std::cout<<"Script will sleep for some time and then check again."<<std::endl;
             }
             else if(processKilled) {
               std::cout<<"Seems like no no other hosts are up this time, let's resume your previously killed process."<<std::endl;
               if(kill(killedProcessID, SIGCONT)==0) {
                 std::cout<<"Successfully Resumed the downloading process."<<std::endl;
               }
               else {
                 std::cout<<"Cannot resume the process."<<std::endl;
               }
             }
             else {
               std::cout<<"Seems like no other host is up for now, have fun! script will check again after sometime."<<std::endl;
             }

         }
         else {
           std::cout<<"A downloading process is not running"<<std::endl;
         }

         usleep(microseconds);
       }


         } else {
           std::cout << "Command processor doesn't exists. Exiting..."<<std::endl;
         }


  return 0;
}
