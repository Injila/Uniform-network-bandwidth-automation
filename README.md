# Uniform-network-bandwidth-automation

This small script is useful for people who need to download heavy stuff often and are conscious about providing uniform internet bandwidth to everyone else in the network.
This script will check if there are multiple hosts up on the network and will terminate the downloading programs on your host. It checks again after sometime,
if no other hosts are up this time, it will relaunch the killed program.